


/* Reklama Banner
============================================================*/
$(document).ready(function () {
	try {
		var owl = $('.rek_bnr');

		owl.owlCarousel({
			// autoPlay: 5000,
			stopOnHover: true,
			singleItem: true,
			autoHeight : true,
			navigation: false,
			pagination: false,
			slideSpeed: 400,
			// transitionStyle : 'fade',
		});

	}
	catch (e) {
		console.warn('Owl Carousel cannot find .rek_bnr');
	}
});
/*========== Reklama Banner ==========*/







/* Client Banner
============================================================*/
$(document).ready(function () {
	try {
		var owl = $('.client_bnr');

		owl.owlCarousel({
			// autoPlay: 5000,
			stopOnHover: true,
			itemsCustom : [
				[0, 2],
				[470, 3],
				[768, 4],
			],
			autoHeight : true,
			navigation: false,
			pagination: false,
			slideSpeed: 400,
			// transitionStyle : 'fade',
		});

	}
	catch (e) {
		console.warn('Owl Carousel cannot find .client_bnr');
	}
});
/*========== Client Banner ==========*/








/* Anchor
============================================================*/
$(document).on('click', '.ftr_info_anchor a', function (e) {
	e.preventDefault();
	
	$('html, body').animate({ scrollTop: 0 }, 700);
});
/*========== Anchor ==========*/








/* Navigation Catalog Menu
============================================================*/
$(document).on('click', '.nav_catalog > li > a', function (e) {
	e.preventDefault();
	
	$(this).parent().toggleClass('active').siblings().removeClass('active').children('.nav_catalog_drop').slideUp();
	$(this).siblings('.nav_catalog_drop').slideToggle();
});
/*========== Navigation Catalog Menu ==========*/








/* Navigation Menu
============================================================*/
$(document).on('click', '.hdr_nav_btn a, .nav_menu_bg, .nav_menu_back', function (e) {
	e.preventDefault();
	
	$('.nav_menu_bx').toggleClass('active');
	$('.nav_menu_bg').toggleClass('active');
	$('html, body').toggleClass('ov-h');
});
/*========== Navigation Menu ==========*/






/* Authorization Telephone
============================================================*/
$(document).on('click', '[data-code]', function (e) {
	e.preventDefault();

	$('[data-code-active]').attr('disabled', true);

	$('[data-code-disabled]').attr('disabled', false).focus();
});
/*========== Authorization Telephone ==========*/










/* Delivery Address Remove
============================================================*/
$(document).on('click', '.dlvr_adrs_it_cls', function () {
	$(this).closest('.dlvr_adrs_it').fadeOut(700, function () {
		$(this).remove();
	});
});
/*========== Delivery Address Remove ==========*/










/* Image Upload
============================================================*/
$('.img_upload > .inp').on('change', function () {
	var elem = $(this);


	if (this.files && this.files[0]) {
		var reader = new FileReader();

		reader.onload = function () {

			elem.parent().addClass('active');
			elem.siblings('.img').attr('src', reader.result);

		};

		reader.readAsDataURL(this.files[0]);
	}
	else {
		console.log('Not uploaded');
	}
});
/*========== Image Upload ==========*/










/* Map Close
============================================================*/
$(document).on('click', '.pharmacy_map_cls a', function (e) {
	e.preventDefault();
	
	$(this).toggleClass('active');
	$('.pharmacy_map').slideToggle();
});
/*========== Map Close ==========*/














/* International Tel Input
============================================================*/
try {
	$('.int_tel').intlTelInput({
		// allowDropdown: false,
		// autoHideDialCode: false,
		// autoPlaceholder: "off",
		// dropdownContainer: "body",
		// excludeCountries: ["us"],
		// formatOnDisplay: false,
		geoIpLookup: function (callback) {
			$.get("http://ipinfo.io", function () {
			}, "jsonp").always(function (resp) {
				var countryCode = (resp && resp.country) ? resp.country : "";
				callback(countryCode);
			});
		},
		// initialCountry: "auto",
		// nationalMode: false,
		onlyCountries: ['uz', 'ru', 'en', 'kz', 'kg', 'kr'],
		// placeholderNumberType: "MOBILE",
		preferredCountries: ['uz'],
		// separateDialCode: true,
		// utilsScript: "lib/int_tel_input/js/utils.js"
	});
}
catch (e) {
	console.warn("International Tel Input cannot find elements");
}
/*========== International Tel Input ==========*/












/* Payment Clarification
============================================================*/
$(document).on('click', '.payment_step_drop', function () {
	$(this).toggleClass('active');
	$('.payment_gr_bx').slideToggle();
});
/*========== Payment Clarification ==========*/












/* PC Cabinet Group
============================================================*/
$(document).on('click', '.pc_cab_gr_ttl', function () {
	$(this).toggleClass('active');
	$(this).siblings('.pc_cab_gr_cnt').slideToggle();
});
/*========== PC Cabinet Group ==========*/













/* Product Group
============================================================*/
$(document).on('click', '.product_one_gr_ttl', function () {
	$(this).toggleClass('active').siblings('.product_one_gr_cnt').slideToggle();
});


$(document).on('click', '.product_one_chem_close a', function (e) {
	e.preventDefault();
	
	$(this).closest('.product_one_gr_cnt').slideToggle().siblings('.product_one_gr_ttl').toggleClass('active');
});
/*========== Product Group ==========*/












/* PC Cabinet Group
============================================================*/
$(document).on('click', '[data-product-filter-title]', function () {
	$(this).toggleClass('active');
	$(this).siblings('[data-product-filter-content]').slideToggle();
});
/*========== PC Cabinet Group ==========*/











/* Price Range Slider
============================================================*/
$(document).ready(function () {
	try {
		var range_slider = $('[data-price-range]');


		// Initialize
		noUiSlider.create(range_slider[0], {
			start: [500, 2200000],
			range: {
				'min': [0, 1000],
				'50%': [100000, 50000],
				'70%': [1000000, 1000000],
				'90%': [10000000, 2000000],
				'max': [20000000]
			},
			// step: 100,
			connect: true,
			format: wNumb({
				decimals: 0,
				thousand: ' ',
				// suffix: ' UZS'
			})
		});




		// Update values of Inputs when Range updates
		range_slider[0].noUiSlider.on('update', function (values, handle) {

			if (handle) {

				// 2 - Button
				range_slider.siblings('.product_all_ftr_prc_val_bx').find('[data-price-range-to]').html(values[handle]);
			} else {

				// 1 - Button
				range_slider.siblings('.product_all_ftr_prc_val_bx').find('[data-price-range-from]').html(values[handle]);
			}
		});
	}
	catch (e) {
		console.warn('noUiSlider cannot find elements');
	}
});
/*========== Price Range Slider ==========*/
















/* Header
============================================================*/

/*========== Header ==========*/


